/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.cooperativa_abc;
import java.sql.*;

/**
 *
 * @author Derick Arzu
 */
public class UserInterface extends javax.swing.JFrame {
    static int num_cuenta_ap = -1;
    static int num_cuenta_ar = -1;
    static MySQL bdd = new MySQL();
    /**
     * Creates new form UserInterface
     */
    public UserInterface() {
        initComponents();
    }
    
    public static void setNumCuenta (int numero_cuenta, int opcion){
        switch(opcion){
            case 1:
                num_cuenta_ap = numero_cuenta;
                break;
            case 2:
                num_cuenta_ar = numero_cuenta;
                break;
        }
    }
    
    public static void setNombreAfiliado (String nombre){
        user_lblnameinfo.setText(nombre);
    }
    
    public static void setCodigoAfiliado (String codigo){
        user_lblcodigoinfo.setText(codigo);
    }
    
    public static void setFechaNacimientoAfiliado (String fechaNacimiento){
        user_lblfechaninfo.setText(fechaNacimiento);
    }
    
    public static String getNombreAfiliado (){
        return user_lblnameinfo.getText();
    }
    
    public static String getCodigoAfiliado (){
        return user_lblcodigoinfo.getText();
    }
    
    public static String getFechaNacimiento (){
        return user_lblfechaninfo.getText();
    }
    
    public static String getInfoCuentas (int numero_cuenta, Connection conn, int opcion){
        PreparedStatement myStmt = null;
        ResultSet myRs = null;
        String info = "";
        try{
            myStmt = conn.prepareStatement("call sp_cuentas_read(?)");
            myStmt.setInt(1, numero_cuenta);
            myRs = myStmt.executeQuery();
            if(myRs.next()){
                switch(opcion){
                    case 1: 
                        info = Integer.toString(myRs.getInt("numero_cuenta"));
                        break;
                    case 2:
                        info = myRs.getString("saldo");
                        break;
                    case 3:
                        info = myRs.getString("fecha_apertura");
                        break;
                }
            }
        }
        catch (Exception exc) {exc.printStackTrace();} 
        return info;
    }
    
    public static int getCantPrestamos(int codigo, Connection conn){
        int cantPrestamos = 0;
        PreparedStatement myStmt = null;
        ResultSet myRs = null;
        try{
            myStmt = conn.prepareStatement("select count(numero_prestamo) from prestamos where codigo = ?");
            myStmt.setInt(1, codigo);
            myRs = myStmt.executeQuery();
            if(myRs.next()){
                cantPrestamos = myRs.getInt("count(numero_prestamo)");
            }
        }
        catch (Exception exc) {exc.printStackTrace();} 
        return cantPrestamos;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        user_lbltitle = new javax.swing.JLabel();
        user_cuentas = new javax.swing.JButton();
        user_lblname = new javax.swing.JLabel();
        user_lblnameinfo = new javax.swing.JLabel();
        user_lblcodigo = new javax.swing.JLabel();
        user_lblcodigoinfo = new javax.swing.JLabel();
        user_lblfechan = new javax.swing.JLabel();
        user_lblfechaninfo = new javax.swing.JLabel();
        user_btnlogout = new javax.swing.JButton();
        user_btnprestamos = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        user_lbltitle.setFont(new java.awt.Font("Gill Sans MT", 1, 24)); // NOI18N
        user_lbltitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        user_lbltitle.setText("¡Bienvenido!");

        user_cuentas.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        user_cuentas.setText("Ver Cuentas");
        user_cuentas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                user_cuentasActionPerformed(evt);
            }
        });

        user_lblname.setFont(new java.awt.Font("Gill Sans MT", 2, 14)); // NOI18N
        user_lblname.setText("Nombre de Afiliado:");

        user_lblnameinfo.setFont(new java.awt.Font("Gill Sans MT", 2, 14)); // NOI18N
        user_lblnameinfo.setPreferredSize(new java.awt.Dimension(14, 17));

        user_lblcodigo.setFont(new java.awt.Font("Gill Sans MT", 2, 14)); // NOI18N
        user_lblcodigo.setText("Codigo de Afiliado:");

        user_lblcodigoinfo.setFont(new java.awt.Font("Gill Sans MT", 2, 14)); // NOI18N
        user_lblcodigoinfo.setPreferredSize(new java.awt.Dimension(14, 17));

        user_lblfechan.setFont(new java.awt.Font("Gill Sans MT", 2, 14)); // NOI18N
        user_lblfechan.setText("Fecha de Nacimiento:");

        user_lblfechaninfo.setFont(new java.awt.Font("Gill Sans MT", 2, 14)); // NOI18N
        user_lblfechaninfo.setToolTipText("");
        user_lblfechaninfo.setPreferredSize(new java.awt.Dimension(14, 17));

        user_btnlogout.setText("Cerrar sesión");
        user_btnlogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                user_btnlogoutActionPerformed(evt);
            }
        });

        user_btnprestamos.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        user_btnprestamos.setText("Ver Prestamos");
        user_btnprestamos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                user_btnprestamosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(user_lbltitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(user_cuentas, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 181, Short.MAX_VALUE)
                .addComponent(user_btnprestamos, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(234, 234, 234)
                        .addComponent(user_btnlogout))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(user_lblcodigo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(user_lblcodigoinfo, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(user_lblname)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(user_lblnameinfo, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(user_lblfechan)
                                .addGap(18, 18, 18)
                                .addComponent(user_lblfechaninfo, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(user_lbltitle)
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(user_lblcodigo)
                    .addComponent(user_lblcodigoinfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(user_lblname)
                    .addComponent(user_lblnameinfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(user_lblfechan)
                    .addComponent(user_lblfechaninfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(user_cuentas)
                    .addComponent(user_btnprestamos))
                .addGap(15, 15, 15)
                .addComponent(user_btnlogout)
                .addGap(29, 29, 29))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void user_cuentasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_user_cuentasActionPerformed
        Connection myConn = bdd.getMySQL();
        Cuentas cuenta = new Cuentas();
        cuenta.setVisible(true);
        cuenta.setLocationRelativeTo(null);
        this.dispose();
        String numero_cuenta_ap = getInfoCuentas(num_cuenta_ap, myConn, 1);
        String saldo_ap = getInfoCuentas(num_cuenta_ap, myConn, 2);
        String fecha_ap = getInfoCuentas(num_cuenta_ap, myConn, 3);
        String numero_cuenta_ar = getInfoCuentas(num_cuenta_ar, myConn, 1);
        String saldo_ar = getInfoCuentas(num_cuenta_ar, myConn, 2);
        String fecha_ar = getInfoCuentas(num_cuenta_ar, myConn, 3);
        cuenta.setCuentaAportaciones(numero_cuenta_ap, saldo_ap, fecha_ap);
        cuenta.setCuentaAhorro(numero_cuenta_ar, saldo_ar, fecha_ar);
        cuenta.setNombreAfiliado(getNombreAfiliado());
        cuenta.setCodigoAfiliado(getCodigoAfiliado());
        cuenta.setFechaNacimientoAfiliado(getFechaNacimiento());
        
    }//GEN-LAST:event_user_cuentasActionPerformed

    private void user_btnlogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_user_btnlogoutActionPerformed
        UserLogin mainScreen = new UserLogin();
        mainScreen.setVisible(true);
        mainScreen.setLocationRelativeTo(null);
        mainScreen.setCurrentUser(" ");
        this.dispose();
    }//GEN-LAST:event_user_btnlogoutActionPerformed

    private void user_btnprestamosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_user_btnprestamosActionPerformed
        Connection myConn = bdd.getMySQL();
        Prestamos prestamo = new Prestamos();
        prestamo.setVisible(true);
        prestamo.setLocationRelativeTo(null);
        this.dispose();
        int codigoAfiliado = Integer.parseInt(getCodigoAfiliado());
        prestamo.setCantPrestamos(getCantPrestamos(codigoAfiliado, myConn));
        prestamo.inicializarArreglo();
        prestamo.getPrestamos(codigoAfiliado, myConn);
        prestamo.getPrestamoActivo(codigoAfiliado, myConn);
        prestamo.setNombreAfiliado(getNombreAfiliado());
        prestamo.setCodigoAfiliado(getCodigoAfiliado());
        prestamo.setFechaNacimientoAfiliado(getFechaNacimiento());
    }//GEN-LAST:event_user_btnprestamosActionPerformed

    /**
     * @param args the command line arguments
     */


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton user_btnlogout;
    private javax.swing.JButton user_btnprestamos;
    private javax.swing.JButton user_cuentas;
    private javax.swing.JLabel user_lblcodigo;
    private static javax.swing.JLabel user_lblcodigoinfo;
    private javax.swing.JLabel user_lblfechan;
    private static javax.swing.JLabel user_lblfechaninfo;
    private javax.swing.JLabel user_lblname;
    private static javax.swing.JLabel user_lblnameinfo;
    private javax.swing.JLabel user_lbltitle;
    // End of variables declaration//GEN-END:variables
}
