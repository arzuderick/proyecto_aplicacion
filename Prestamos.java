/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.cooperativa_abc;
import java.sql.*;
import static my.cooperativa_abc.Cuentas.nombreAfiliado;
/**
 *
 * @author Derick Arzu
 */
public class Prestamos extends javax.swing.JFrame {
    static MySQL bdd = new MySQL();
    public static int cantPrestamos = 0;
    static Object [][]arreglo;
    static String nombreAfiliado = "";
    static String codigoAfiliado = "";
    static String fechaNacimiento = "";
    /**
     * Creates new form Prestamos
     */
    public Prestamos() {
        initComponents();
    }
    
    public static void inicializarArreglo(){
        arreglo = new Object [cantPrestamos][6];
        prestamos_tblread.setModel(new javax.swing.table.DefaultTableModel(
            arreglo,
            new String [] {
                "Numero de Prestamo", "Monto", "Periodos", "Fecha", "Saldo", "Tipo"
            }
        ));
        
        prestamos_scrlread.setViewportView(prestamos_tblread);
    }
    
    public static void setCantPrestamos(int cant){
        cantPrestamos = cant;
    }
    
    public static void setNombreAfiliado (String nombre){
        nombreAfiliado = nombre;
    }
    
    public static void setCodigoAfiliado (String codigo){
        codigoAfiliado = codigo;
    }
    
    public static void setFechaNacimientoAfiliado (String fechaNacimiento_){
        fechaNacimiento = fechaNacimiento_;
    }
    
    public static void setPorPagar(String porPagar){
        prestamos_lblporpagarinfo.setText(porPagar);
    }
    
    public static void setMonto(String monto){
        prestamos_lblmontoinfo.setText(monto);
    }
    
    public static String getPorPagar(){
        return prestamos_lblporpagarinfo.getText();
    }
    
    public static String getMonto(){
        return prestamos_lblmontoinfo.getText();
    }
    
    public static void getPrestamos(int codigo, Connection conn){
        PreparedStatement myStmt = null;
        ResultSet myRs = null;
        try{
            myStmt = conn.prepareStatement("select numero_prestamo, monto, periodos, fecha, saldo, tipo from prestamos where codigo = ?");
            myStmt.setInt(1, codigo);
            myRs = myStmt.executeQuery();
            int i = 0;
            while(myRs.next()){
                    prestamos_tblread.getModel().setValueAt(myRs.getInt("numero_prestamo"), i, 0);
                    prestamos_tblread.getModel().setValueAt(myRs.getFloat("monto"), i, 1);
                    prestamos_tblread.getModel().setValueAt(myRs.getInt("periodos"), i, 2);
                    prestamos_tblread.getModel().setValueAt(myRs.getString("fecha"), i, 3);
                    prestamos_tblread.getModel().setValueAt(myRs.getFloat("saldo"), i, 4);
                    if(myRs.getInt("tipo") == 1){
                        prestamos_tblread.getModel().setValueAt("Fiduciario", i, 5);
                    }
                    else if (myRs.getInt("tipo") == 2)
                        prestamos_tblread.getModel().setValueAt("Automático", i, 5);
                i++;
            }
        }
        catch (Exception exc) {exc.printStackTrace();} 
    }
    
    public static void getPrestamoActivo(int codigo, Connection conn){
        PreparedStatement activoStmt = null;
        ResultSet activoRs = null;
        try{
            activoStmt = conn.prepareStatement("select saldo, monto from prestamos where codigo = ? and activo = 1");
            activoStmt.setInt(1, codigo);
            activoRs = activoStmt.executeQuery();
            if(activoRs.next()){
                float saldo = activoRs.getFloat("saldo");
                float monto = activoRs.getFloat("monto");
                setPorPagar(Float.toString(saldo));
                setMonto(Float.toString(monto));
            }
        }
        catch (Exception exc) {exc.printStackTrace();} 
    }
    
    public static int getNumeroPrestamo (int codigo, Connection conn){
        int numero_prestamo = -1;
        PreparedStatement myStmt = null;
        ResultSet myRs = null;
        try{
            myStmt = conn.prepareStatement("select numero_prestamo from prestamos where codigo = ? and activo = 1");
            myStmt.setInt(1, codigo);
            myRs = myStmt.executeQuery();
            if(myRs.next()){
                numero_prestamo = myRs.getInt("numero_prestamo");
            }
        }
        catch (Exception exc) {exc.printStackTrace();}
        return numero_prestamo;
    }
    
    public static int getCantPagos(int numero_prestamo, Connection conn){
        int cantPagos = 0;
        PreparedStatement myStmt = null;
        ResultSet myRs = null;
        try{
            myStmt = conn.prepareStatement("select count(numero_pago) from pagos where numero_prestamo = ?");
            myStmt.setInt(1, numero_prestamo);
            myRs = myStmt.executeQuery();
            if(myRs.next()){
                cantPagos = myRs.getInt("count(numero_pago)");
            }
        }
        catch (Exception exc) {exc.printStackTrace();} 
        return cantPagos;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        prestamos_title = new javax.swing.JLabel();
        prestamos_scrlread = new javax.swing.JScrollPane();
        prestamos_tblread = new javax.swing.JTable();
        prestamos_btnatras = new javax.swing.JButton();
        prestamos_lblactivo = new javax.swing.JLabel();
        prestamos_lblporpagar = new javax.swing.JLabel();
        prestamos_lblmonto = new javax.swing.JLabel();
        prestamos_lblporpagarinfo = new javax.swing.JLabel();
        prestamos_lblmontoinfo = new javax.swing.JLabel();
        prestamo_btnpagos = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        prestamos_title.setFont(new java.awt.Font("Gill Sans MT", 1, 24)); // NOI18N
        prestamos_title.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        prestamos_title.setText("Prestamos");

        prestamos_tblread.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6"
            }
        ));
        prestamos_scrlread.setViewportView(prestamos_tblread);

        prestamos_btnatras.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        prestamos_btnatras.setText("Atrás");
        prestamos_btnatras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prestamos_btnatrasActionPerformed(evt);
            }
        });

        prestamos_lblactivo.setFont(new java.awt.Font("Gill Sans MT", 2, 18)); // NOI18N
        prestamos_lblactivo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        prestamos_lblactivo.setText("Información de Prestamo Activo");

        prestamos_lblporpagar.setFont(new java.awt.Font("Gill Sans MT", 2, 16)); // NOI18N
        prestamos_lblporpagar.setText("Por pagar: ");

        prestamos_lblmonto.setFont(new java.awt.Font("Gill Sans MT", 2, 16)); // NOI18N
        prestamos_lblmonto.setText("Monto");

        prestamos_lblporpagarinfo.setFont(new java.awt.Font("Gill Sans MT", 2, 16)); // NOI18N
        prestamos_lblporpagarinfo.setPreferredSize(new java.awt.Dimension(14, 19));

        prestamos_lblmontoinfo.setFont(new java.awt.Font("Gill Sans MT", 2, 16)); // NOI18N
        prestamos_lblmontoinfo.setToolTipText("");
        prestamos_lblmontoinfo.setPreferredSize(new java.awt.Dimension(14, 19));

        prestamo_btnpagos.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        prestamo_btnpagos.setText("Ver Pagos");
        prestamo_btnpagos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prestamo_btnpagosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(prestamos_title, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(prestamos_lblactivo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(prestamos_btnatras)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(prestamo_btnpagos)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(prestamos_lblporpagar)
                                .addGap(18, 18, 18)
                                .addComponent(prestamos_lblporpagarinfo, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(prestamos_lblmonto)
                                    .addGap(44, 44, 44)
                                    .addComponent(prestamos_lblmontoinfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGap(231, 231, 231))
                                .addComponent(prestamos_scrlread, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(60, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(prestamos_title)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(prestamos_scrlread, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(prestamos_lblactivo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(prestamos_lblporpagar)
                    .addComponent(prestamos_lblporpagarinfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(prestamos_lblmonto)
                    .addComponent(prestamos_lblmontoinfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(prestamos_btnatras)
                    .addComponent(prestamo_btnpagos))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void prestamos_btnatrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prestamos_btnatrasActionPerformed
        UserInterface u_interface = new UserInterface();
        u_interface.setVisible(true);
        u_interface.setLocationRelativeTo(null);                
        this.dispose();
        u_interface.setNombreAfiliado(nombreAfiliado);
        u_interface.setCodigoAfiliado(codigoAfiliado);
        u_interface.setFechaNacimientoAfiliado(fechaNacimiento);
    }//GEN-LAST:event_prestamos_btnatrasActionPerformed

    private void prestamo_btnpagosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prestamo_btnpagosActionPerformed
       Connection myConn = bdd.getMySQL();
       Pagos pagos = new Pagos();
       pagos.setVisible(true);
       pagos.setLocationRelativeTo(null);
       this.dispose();
       int codAfiliado = Integer.parseInt(codigoAfiliado);
       int numero_prestamo = getNumeroPrestamo(codAfiliado, myConn);
       System.out.println(numero_prestamo);
       pagos.setCodigoAfiliado(codAfiliado);
       pagos.setCantPrestamos(cantPrestamos);
       pagos.setCantPagos(getCantPagos(numero_prestamo, myConn));
       pagos.inicializarArreglo();
       pagos.getPagos(numero_prestamo, myConn);
    }//GEN-LAST:event_prestamo_btnpagosActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton prestamo_btnpagos;
    private javax.swing.JButton prestamos_btnatras;
    private javax.swing.JLabel prestamos_lblactivo;
    private javax.swing.JLabel prestamos_lblmonto;
    private static javax.swing.JLabel prestamos_lblmontoinfo;
    private javax.swing.JLabel prestamos_lblporpagar;
    private static javax.swing.JLabel prestamos_lblporpagarinfo;
    private static javax.swing.JScrollPane prestamos_scrlread;
    private static javax.swing.JTable prestamos_tblread;
    private javax.swing.JLabel prestamos_title;
    // End of variables declaration//GEN-END:variables
}
